#pragma once

#include "HeadRing.h"

THeadRing::THeadRing() :TDatList() 
{
	InsLast();
	pHead = pFirst;
	pStop = pHead;
	Reset();
	ListLen = 0;
	pFirst->SetNextLink(pFirst);
}

THeadRing:: ~THeadRing() 
{
	DelList();
	DelLink(pHead);
	pHead = nullptr;
}


void THeadRing::InsFirst(PTDatValue pVal)
{
	TDatList::InsFirst(pVal);
	pHead->SetNextLink(pFirst);
}

void THeadRing::DelFirst(void) 
{
	TDatList::DelFirst();
	pHead->SetNextLink(pFirst);
}