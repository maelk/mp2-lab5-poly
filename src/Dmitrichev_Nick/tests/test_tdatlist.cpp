#pragma once

#include "gtest.h"
#include "DatList.h"

typedef struct ValueInt :TDatValue
{
	int Value;
	ValueInt() { Value = 0; }
	virtual TDatValue * GetCopy() { TDatValue *temp = new ValueInt; return temp; }
} *PValueInt;

//Initialization//////////////////////////////////////////////////////////////////
class TestDatList : public ::testing::Test
{
protected:
	virtual void SetUp()
	{
		// Testing only elem
		Val = new ValueInt();
		Val->Value = 100;

		// Testing ten elems
		ArrVal = new PValueInt[N];
		for (int i = 0; i < N; i++)
		{
			ArrVal[i] = new ValueInt();
			ArrVal[i]->Value = i;
		}
	}
	virtual void TearDown()
	{
		delete ArrVal;
	}
	const int N = 10;
	PValueInt Val;
	PValueInt *ArrVal;
	TDatList List;
};

TEST_F(TestDatList, new_list_has_null_size)
{
	// NO ACTIONS

	EXPECT_EQ(List.GetListLength(), 0);
}

TEST_F(TestDatList, reset_makes_current_position_equil_null)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.Reset();

	EXPECT_EQ(List.GetCurrentPos(), 0);
}

TEST_F(TestDatList, setcurrentpos_changes_the_current_position)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.SetCurrentPos(5);

	EXPECT_EQ(List.GetCurrentPos(), 5);
}

TEST_F(TestDatList, setcurrentpos_can_change_position_if_it_is_more_than_size_of_list)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);

	EXPECT_ANY_THROW(List.SetCurrentPos(12));
}

TEST_F(TestDatList, size_of_the_list_is_correct_after_inserting)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);

	EXPECT_EQ(List.GetListLength(), N);
}

TEST_F(TestDatList, size_of_the_list_is_correct_after_N_times_removing)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	for (int i = 0; i < N; i++)
		List.DelFirst();

	EXPECT_EQ(List.GetListLength(), 0);
}

TEST_F(TestDatList, size_of_the_list_is_correct_after_removing_of_all_list)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.DelList();

	EXPECT_EQ(List.GetListLength(), 0);
}

TEST_F(TestDatList, cant_delete_elem_if_size_equil_null)
{
	// NO ACTIONS

	EXPECT_ANY_THROW(List.DelFirst());
	EXPECT_ANY_THROW(List.DelCurrent());
}

TEST_F(TestDatList, list_is_ended_if_current_position_equil_length_of_list)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.Reset();
	List.SetCurrentPos(List.GetListLength());

	EXPECT_TRUE(List.IsListEnded());
}


TEST_F(TestDatList, can_add_an_item_to_the_end)
{
	for (int i = 0; i < N - 1; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8
	List.InsLast(Val);
	// 0 1 2 3 4 5 6 7 8 100
	List.SetCurrentPos(List.GetListLength()-1);

	EXPECT_EQ(((PValueInt)(List.GetDatValue()))->Value,Val->Value);
}

TEST_F(TestDatList, can_add_an_item_to_the_beginning)
{
	for (int i = 0; i < N - 1; i++)
		List.InsFirst(ArrVal[i]);
	// 8 7 6 5 4 3 2 1 0
	List.InsFirst(Val);
	// 100 8 7 6 5 4 3 2 1 0

	EXPECT_EQ(((PValueInt)(List.GetDatValue()))->Value, Val->Value);
}

TEST_F(TestDatList, can_add_an_item_to_the_current_position)
{
	for (int i = 0; i < N-1; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8
	// Current = 0
	List.SetCurrentPos(5);
	// Current = 5
	List.InsCurrent(Val);
	// 0 1 2 3 4 100 5 6 7 8

	EXPECT_EQ(((PValueInt)(List.GetDatValue()))->Value, Val->Value);
}

TEST_F(TestDatList, can_add_ten_elems_to_the_end)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8 9

	for (int i = 0; i < N; i++, List.GoNext())
		EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value);
}

TEST_F(TestDatList, can_add_ten_elems_to_the_beginnig)
{
	for (int i = 0; i < N; i++)
		List.InsFirst(ArrVal[i]);
	// 9 8 7 6 5 4 3 2 1 0

	for (int i = N - 1; i > 0; i--, List.GoNext())
		EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value);
}

TEST_F(TestDatList, can_add_ten_elems_to_the_current_pos)
{
	for (int i = 0; i < 5; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4
	// Current = 0
	List.GoNext();
	List.GoNext();
	// Current = 2
	List.InsCurrent(Val);
	// 0 1 100 2 3 4

	List.Reset();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[0]->Value);
	List.GoNext();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[1]->Value);
	List.GoNext();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, Val->Value);
	List.GoNext();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[2]->Value);
}

TEST_F(TestDatList, can_delete_elem_from_beginnig)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8 9
	List.DelFirst();
	// 1 2 3 4 5 6 7 8 9

	EXPECT_EQ(((PValueInt)(List.GetDatValue()))->Value, ArrVal[1]->Value);
	EXPECT_EQ(List.GetListLength(), N - 1);
}

TEST_F(TestDatList, can_delete_elem_from_the_current_pos)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8 9
	List.GoNext();
	List.DelCurrent();
	// 0 2 3 4 5 6 7 8 9

	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[2]->Value);
	EXPECT_EQ(List.GetListLength(), N - 1);
}