﻿# Отчёт 

## по самостоятельной работе №5 по дисциплине "Алгоритмы и Структуры Данных"

# тема:  "Полиномы"


## Требования к лабораторной работе

В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное представление полиномов и выполнение следующих операций над ними:

 - ввод полинома;
 - организация хранения полинома;
 - удаление введенного ранее полинома;
 - копирование полинома;
 - сложение двух полиномов;
 - вычисление значения полинома при заданных значениях переменных;
 - вывод.

Предполагается, что в качестве структуры хранения будут использоваться списки. В качестве дополнительной цели в лабораторной работе ставится также задача разработки некоторого общего представления списков и операций по их обработке. В числе операций над списками должны быть реализованы следующие действия:

 - поддержка понятия текущего звена;
 - вставка звеньев в начало, после текущей позиции и в конец списков;
 - удаление звеньев в начале и в текущей позиции списков;
 - организация последовательного доступа к звеньям списка (итератор).

В ходе выполнения лабораторной работы должно быть выполнено сопоставление разработанных средств работы со списками с возможностями работы со списками в библиотеке STL.

## Условия и ограничения

При выполнении лабораторной работы можно использовать следующие основные предположения:

 1. Разработка структуры хранения должна быть ориентирована на представление полиномов от трех неизвестных.
 2. Степени переменных полиномов не могут превышать значения 9, т.е. 0 <= i, j, k <= 9.
 3. Число мономов в полиномах существенно меньше максимально возможного количества (тем самым, в структуре хранения должны находиться только мономы с ненулевыми коэффициентами).

## Структуры хранения полиномов

Для представления полиномов могут быть выбраны различные структуры хранения. Критериями выбора структуры хранения являются размер требуемой памяти и сложность (трудоемкость) реализации операций над полиномами.

Возможный вариант структуры хранения – использование массивов (в случае полиномов от трех переменных – трехмерная матрица коэффициентов полинома). Такой способ обеспечивает простую реализацию операций над полиномами, но он не эффективен в части объема требуемой памяти. Так, при сделанных допущениях для хранения одного полинома в массиве потребуется порядка 8000 байт памяти – при этом в памяти будут храниться в основном параметры мономов с нулевыми коэффициентами.

Разработка более эффективной структуры хранения должна быть выполнена с учетом следующих рекомендаций:

 - в структуре хранения должны храниться данные только для мономов с ненулевыми коэффициентами; 
 - порядок размещения данных в структуре хранения должен обеспечивать возможность быстрого поиска мономов с заданными свойствами (например, для приведения подобных).

Так как степени переменных мономов ограничены и принимают значения от 0 до 9, то можно использовать ОДНО число, которое будет принимать значения от 0 до 999. Причем, если A, B и C - степени, то это число равно ABC = A*100+B*10+C. 
	
Данное соответствие является взаимно-однозначным. Обратное соответствие определяется при помощи выражений
A=E(ABC%100),  B=E(ABC-A*100)%10,  C=ABC-A*100-B*10.

Наиболее эффективным способом организации структуры хранения полиномов являются линейный (односвязный) список. Тем самым, в рамках лабораторной работы появляется подзадача – разработка структуры хранения в виде линейных списков. 

## Алгоритмы

### Списки

Для работы со списками реализуются следующие операции:

 - методы получения параметров состояния списка (проверка на пустоту, получение текущего количества звеньев);
 - метод доступа к значению первого, текущего или последнего звена;
 - методы навигации по списку (итератор);
 - методы вставки перед первым, после текущего и последнего звеньев;
 - методы удаления первого и текущего звена.

### Полиномы

Для работы с полиномами реализуются следующие операции:

 - конструкторы инициализации и копирования;
 - метод присваивания;
 - метод сложения полиномов.

## Структура

С учетом всех перечисленных ранее требований может быть предложен следующий состав классов и отношения между этими классами:

 - класс TDatValue для определения класса объектов-значений списка (абстрактный класс);
 - класс TMonom для определения объектов-значений параметров монома;
 - класс TRootLink для определения звеньев (элементов) списка (абстрактный класс);
 - класс TDatLink для определения звеньев (элементов) списка с указателем на объект-значение;
 - класс TDatList для определения линейных списков;
 - класс THeadRing для определения циклических списков с заголовком;
 - класс TPolinom для определения полиномов.
На рисунке ниже показаны также отношения между классами: обычными стрелками показаны отношения наследования (базовый класс – производный класс), а ромбовидными стрелками – отношения ассоциации (класс-владелец – класс-компонент).

![](http://i.imgur.com/h5Mm9EV.png)

В соответствии с предложенной структурой классов модульная (файловая) структура программной системы может иметь вид:

 - DatValue.h – модуль, объявляющий абстрактный класс объектов-значений списка;
 - RootLink.h, RootLink.cpp – модуль базового класса для звеньев (элементов) списка;
 - DatLink.h, DatLink.cpp – модуль класса для звеньев (элементов) списка с указателем на объект-значение;
 - DatList.h, DatList.cpp – модуль класса линейных списков;
 - HeadRing.h, HeadRing.cpp – модуль класса циклических списков с заголовком;
 - Monom.h, Monom.cpp – модуль класса моном;
 - Polinom.h, Polinom.cpp – модуль класса полиномов;
 - test_polinom.cpp, test_tdatlist.cpp – модуль программы тестирования.

## Реализация

### Файл `DatValue.h`

```c++

#pragma once
#include <iostream>

class TDatValue;
typedef TDatValue* PTDatValue;

class TDatValue {
public:
	virtual TDatValue * GetCopy() = 0; // создание копии
	~TDatValue() {}
};

```

### Файл `RootLink.h`

```c++

#pragma once

class TRootLink;
typedef TRootLink *PTRootLink;

class TRootLink {
protected:
	PTRootLink  pNext;  // указатель на следующее звено
public:
	TRootLink(PTRootLink pN = NULL) { pNext = pN; }
	PTRootLink  GetNextLink() { return  pNext; }
	void SetNextLink(PTRootLink  pLink) { pNext = pLink; }
	void InsNextLink(PTRootLink  pLink) 
	{
		PTRootLink p = pNext;
		pNext = pLink;
		if (pLink != NULL)
			pLink->pNext = p;
	}
	virtual void       SetDatValue(PTDatValue pVal) = 0;
	virtual PTDatValue GetDatValue() = 0;

	friend class TDatList;
};

```

### Файл `DatLink.h`

```c++

#pragma once

#include "DatValue.h"
#include "RootLink.h"

class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink {
protected:
	PTDatValue pValue;  // указатель на объект значения
public:
	TDatLink(PTDatValue pVal = NULL, PTRootLink pN = NULL) :
		TRootLink(pN) {
		pValue = pVal;
	}
	void       SetDatValue(PTDatValue pVal) { pValue = pVal; }
	PTDatValue GetDatValue() { return  pValue; }
	PTDatLink  GetNextDatLink() { return  (PTDatLink)pNext; }
	friend class TDatList;
};

```

### Файл `DatList.h`

```c++

#pragma once
#include "DatLink.h"

#define Error_1000 1000  // SetCurrentPos( pos ), where pos is more than length of list
#define Error_1001 1001  // Removing of the link can't execute if length of list is null
enum TLinkPos { FIRST, CURRENT, LAST };

class TDatList : public TDatLink {
protected:
	PTDatLink pFirst;    // первое звено
	PTDatLink pLast;     // последнее звено
	PTDatLink pCurrLink; // текущее звено
	PTDatLink pPrevLink; // звено перед текущим
	PTDatLink pStop;     // значение указателя, означающего конец списка
	int CurrPos;         // номер текущего звена (нумерация от 0)
	int ListLen;         // количество звеньев в списке
protected:  // методы
	PTDatLink GetLink(PTDatValue pVal = nullptr, PTDatLink pLink = nullptr);
	void      DelLink(PTDatLink pLink);   // удаление звена
public:
	TDatList();
	~TDatList() { DelList(); }
	// доступ
	//using TDatLink::GetDatValue;
	PTDatValue GetDatValue(TLinkPos mode = CURRENT) const; // значение
	virtual int IsEmpty()  const { return pFirst == pStop; } // список пуст ?
	int GetListLength()    const { return ListLen; }       // к-во звеньев
														   // навигация
	void SetCurrentPos(int pos);          // установить текущее звено
	int GetCurrentPos(void) const;       // получить номер тек. звена
	virtual void Reset(void);            // установить на начало списка
	virtual bool IsListEnded(void) const; // список завершен ?
	int GoNext(void);                    // сдвиг вправо текущего звена
										 // (=1 после применения GoNext для последнего звена списка)
										 // вставка звеньев
	virtual void InsFirst(PTDatValue pVal = nullptr); // перед первым
	virtual void InsLast(PTDatValue pVal = nullptr); // вставить последним
	virtual void InsCurrent(PTDatValue pVal = nullptr); // перед текущим
														// удаление звеньев
	virtual void DelFirst(void);    // удалить первое звено
	virtual void DelCurrent(void);    // удалить текущее звено
	virtual void DelList(void);    // удалить весь список
};

```

### Файл `DatList.cpp`

```c++

#pragma once

#include "DatList.h"

//protected//////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink) // getting of a new link
{
	return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)	// removal of the link
{
	if (pLink != nullptr)
	{
		if (pLink->pValue != nullptr)
			delete pLink->pValue;		// removal of a value in the link
		delete pLink;					// removal of the link
	}
}   

//public/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

TDatList::TDatList()
{
	ListLen = 0;
	pFirst = pCurrLink = pLast = nullptr;
	Reset();
}

void TDatList::Reset(void) // set in the beginning of the list 
{
	pPrevLink = pStop;  // pCurrLink has to be equal to beginning of the all list
	if (!IsEmpty())		// but the list can be not empty
	{
		pCurrLink = pFirst;
		CurrPos = 0;
	}
	else				// and can be empty
	{
		pCurrLink = pStop;
		CurrPos = -1;
	}
}             

//access/////////////////////////////////////////////////////////////////////////

PTDatValue TDatList::GetDatValue(TLinkPos mode) const // get a value of a link
{
	PTDatLink tmp = nullptr;
	switch (mode)
	{
	case FIRST:
		tmp = pFirst;
		break;
	case LAST:
		tmp = pLast;
		break;
	default:
		tmp = pCurrLink;
		break;
	
	}
	if (tmp != nullptr)
		return tmp->TDatLink::GetDatValue();
	else
		return nullptr;
}

//navigation/////////////////////////////////////////////////////////////////////

int TDatList::GoNext(void) // shift to the right the current link
{
	if (!IsListEnded()) 
	{
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
		return 1;
	}
	else
		return 0;
}

void TDatList::SetCurrentPos(int pos) // set the current link
{
	if (pos <= GetListLength())
	{
		Reset();
		for (int i = 0; i < pos; i++, GoNext()) {}
	}
	else
		throw Error_1000;
}

int TDatList::GetCurrentPos(void) const // get the CurrPos 
{
	return CurrPos;
}

bool TDatList::IsListEnded(void) const  // the list ended or not
{
	return pCurrLink == pStop; 
}

//insert/////////////////////////////////////////////////////////////////////////

void TDatList::InsFirst(PTDatValue pVal) // insert before the first link
{
	PTDatLink tmp = GetLink(pVal, pFirst);
	if (tmp) // if it isn't equal nullptr
	{
		tmp->SetNextLink(pFirst);
		pFirst = tmp;
		ListLen++;
		if (ListLen == 1) 
		{
			pLast = tmp;
			Reset();
		}
		else  if (CurrPos == 0)
			pCurrLink = tmp;
		else
			CurrPos++;
	}
}
void TDatList::InsLast(PTDatValue pVal) // insert the last
{
	PTDatLink tmp = GetLink(pVal, pStop);
	if (tmp)		// if it isn't equal nullptr
	{
		if (pLast)	// if it isn't equal nullptr
			pLast->SetNextLink(tmp);
		pLast = tmp;
		ListLen++;
		if (ListLen == 1) 
		{
			pFirst = tmp;
			Reset();
		}
		if (IsListEnded())
			pCurrLink = tmp;
	}
}

void TDatList::InsCurrent(PTDatValue pVal) // insert before the current link
{
	if ((pCurrLink == pFirst) || IsEmpty())
		InsFirst(pVal);
	else 
	{
		if (IsListEnded())
			InsLast(pVal);
		else 
		{
			PTDatLink tmp = GetLink(pVal, pCurrLink);
			if (tmp) // if it isn't equal nullptr
			{
				pPrevLink->SetNextLink(tmp);
				tmp->SetNextLink(pCurrLink);
				ListLen++;
				pCurrLink = tmp;
			}
		}
	}
}

//delete & removal///////////////////////////////////////////////////////////////

void TDatList::DelFirst(void) // remove the first link
{
	if (!IsEmpty())
	{
		PTDatLink tmp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(tmp);
		ListLen--;

		if (IsEmpty())
		{
			pLast = pStop;
			Reset();
		}
		else if (CurrPos == 1)	// if the first link was previous for the current link
			pPrevLink = pStop;
		else if (CurrPos == 0)	// if the first link was the current link
			pCurrLink = pFirst;
		if (CurrPos)			// after removing of the first link the current link has to be 1 less
			CurrPos--;
	}
	else
		throw Error_1001;
} 
void TDatList::DelCurrent(void) // remove the current link
{
	if (pCurrLink)								// if it isn't equal nullptr
	{
		if ((pCurrLink == pFirst) || IsEmpty())	// remove the first link was higher
			DelFirst();
		else
		{
			PTDatLink tmp = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			pPrevLink->SetNextLink(pCurrLink);
			DelLink(tmp);
			ListLen--;

			if (pCurrLink == pLast)
			{
				pLast = pPrevLink;
				pCurrLink = pStop;
			}
		}
	}
	else
		throw Error_1001;
}
void TDatList::DelList(void) // remove the all list
{
	while (!IsEmpty())
		DelFirst();
	CurrPos = -1;
	pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
} 

```

### Файл `HeadRing.h`

```c++

#pragma once

#include "DatList.h"

class THeadRing : public TDatList {
protected:
	PTDatLink pHead;     // заголовок, pFirst - звено за pHead
public:
	THeadRing();
	~THeadRing();
	// вставка звеньев
	virtual void InsFirst(PTDatValue pVal = NULL); // после заголовка
												   // удаление звеньев
	virtual void DelFirst(void);                 // удалить первое звено
};

```

### Файл `HeadRing.cpp`

```c++

#pragma once

#include "HeadRing.h"

THeadRing::THeadRing() :TDatList() 
{
	InsLast();
	pHead = pFirst;
	pStop = pHead;
	Reset();
	ListLen = 0;
	pFirst->SetNextLink(pFirst);
}

THeadRing:: ~THeadRing() 
{
	DelList();
	DelLink(pHead);
	pHead = nullptr;
}


void THeadRing::InsFirst(PTDatValue pVal)
{
	TDatList::InsFirst(pVal);
	pHead->SetNextLink(pFirst);
}

void THeadRing::DelFirst(void) 
{
	TDatList::DelFirst();
	pHead->SetNextLink(pFirst);
}

```

### Файл `Monom.h`

```c++

#pragma once

#include "DatValue.h"
#include <iostream>		// using for printing the monoms
#include <string>		// the same

using namespace std;	// there is cout is using (below)

class TMonom;
typedef TMonom* PTMonom;

class TMonom : public TDatValue 
{
protected:
	int Coeff; // coefficient of the monom
	int Index; // convolution of the degrees
public:
	TMonom(int cval = 1, int ival = 0);
	virtual TDatValue * GetCopy();
	void SetCoeff(int);
	int  GetCoeff(void);
	void SetIndex(int);
	int  GetIndex(void);
	TMonom& operator=(const TMonom &);
	bool operator==(const TMonom &)const;
	bool operator<(const TMonom &)const;
	double calculate(double, double, double);
	friend ostream& operator<<(ostream &, TMonom &);
	friend class TPolinom;
};

```

### Файл `Monom.cpp`

```c++

#pragma once

#include "Monom.h"


TMonom::TMonom(int cval, int ival) 
{
	Coeff = cval;
	Index = ival;
}
TDatValue * TMonom::GetCopy()  // make a copy of the monom
{
	TDatValue * temp = new TMonom(Coeff, Index); 
	return temp;
}
void TMonom::SetCoeff(int cval) 
{ 
	Coeff = cval;
}
int   TMonom::GetCoeff(void)
{ 
	return Coeff;
}
void  TMonom::SetIndex(int ival)
{ 
	Index = ival; 
}
int   TMonom::GetIndex(void)
{ 
	return Index;
}
TMonom&  TMonom::operator=(const TMonom &tm)
{
	Coeff = tm.Coeff; 
	Index = tm.Index;
	return *this;
}
bool  TMonom::operator==(const TMonom &tm)const
{
	return (Coeff == tm.Coeff) && (Index == tm.Index);
}
bool  TMonom::operator<(const TMonom &tm)const
{
	return Index<tm.Index;
}
double  TMonom::calculate(double x, double y, double z)
{
	int indx = Index / 100;
	int indy = (Index % 100) / 10;
	int indz = Index % 10;
	return Coeff*pow(x, indx)*pow(y, indy)*pow(z, indz);
}
ostream&  operator<<(ostream &os, TMonom &tm)
{
	string str = "";
	if (tm.Coeff < 0)
		str += '-';
	else
		str += '+';
	str += to_string(tm.Coeff);
	if (tm.Index != 0)
	{
		int indx = tm.Index / 100;
		int indy = (tm.Index % 100) / 10;
		int indz = tm.Index % 10;
		if (indx != 0)
			if (indx != 1)
				str += ("*x^" + to_string(indx));
			else
				str += "*x";
		if (indy != 0)
			if (indy != 1)
				str += ("*y^" + to_string(indy));
			else
				str += "*x";
		if (indz != 0)
			if (indz != 1)
				str += ("*z^" + to_string(indz));
			else
				str += "*z";
	}
	os << str << ' ';
	return os;
}

```

### Файл `Polinom.h`

```c++

#pragma once

#include "HeadRing.h"
#include "Monom.h"

class TPolinom : public THeadRing {
private:
	void update(void);
public:
	TPolinom(int monoms[][2] = NULL, int km = 0); // конструктор
												  // полинома из массива «коэффициент-индекс»
	TPolinom(TPolinom &q);      // конструктор копирования
	PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }
	TPolinom & operator+(TPolinom &q); // сложение полиномов
	TPolinom & operator=(TPolinom &q); // присваивание
	bool operator==(TPolinom &q); // сравнение
	double calculate(double, double, double);
	friend ostream & operator<<(ostream & os, TPolinom & q); // перегрузка вывода
};

```

### Файл `Polinom.cpp`

```c++

#pragma once

#include "Polinom.h"

//private////////////////////////////////////////////////////////////////////////


void TPolinom::update(void)
{
	int pos = -1;
	PTMonom curMon, polMon;
	TPolinom tempPol(*this);
	for (Reset();!IsListEnded();GoNext())
	{
		curMon = (PTMonom)GetDatValue();
		tempPol.Reset();
		while (!tempPol.IsListEnded())
		{
			if (GetCurrentPos() != tempPol.GetCurrentPos())
			{
				polMon = (PTMonom)tempPol.GetDatValue();
				if (curMon->Index == polMon->Index && curMon->Index != -1 && polMon->Index != -1)
				{
					pos = GetCurrentPos();
					curMon->Coeff += polMon->Coeff;

					SetCurrentPos(tempPol.GetCurrentPos());
					DelCurrent();
					tempPol.DelCurrent();
					SetCurrentPos(pos);
					tempPol.Reset();
				}
			}
			tempPol.GoNext();
		}
	}
	for (Reset();!IsListEnded();GoNext())
		if (((PTMonom)GetDatValue())->Coeff == 0)
			DelCurrent();
	Reset();
}

//constructers///////////////////////////////////////////////////////////////////

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom elem = new TMonom(0, -1);
	pHead->SetDatValue(elem);
	for (int i = 0; i < km; i++) 
	{
		elem = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(elem);
	}
	update();
}

TPolinom::TPolinom(TPolinom &q)
{
	PTMonom elem = new TMonom(0, -1);
	pHead->SetDatValue(elem);
	int tempPosition = q.GetCurrentPos();
	for (q.Reset(); !q.IsListEnded(); q.GoNext()) 
	{
		elem = q.GetMonom();
		InsLast(elem->GetCopy());
	}
	q.SetCurrentPos(tempPosition);
}

//operators//////////////////////////////////////////////////////////////////////

TPolinom& TPolinom::operator+(TPolinom &q)
{
	PTMonom this_mon, q_mon, new_mon;
	q.Reset();
	Reset();
	TPolinom* result=new TPolinom(*this);

	while (true)
	{
		this_mon = result->GetMonom();
		q_mon = q.GetMonom();
		if ((this_mon->operator< (*q_mon)))
		{
			new_mon = new TMonom(q_mon->Coeff, q_mon->Index);
			result->InsCurrent(new_mon);
			q.GoNext();
		}
		else if ((q_mon->operator< (*this_mon)))
			result->GoNext();
		else
		{
			if (this_mon->Index == -1)
				break;
			this_mon->Coeff += q_mon->Coeff;
			if (this_mon->Coeff != 0) {
				result->GoNext();
				q.GoNext();
			}
			else
			{
				result->DelCurrent();
				q.GoNext();
			}
		}
	}
	result->update();
	return *result;
}
TPolinom& TPolinom::operator=(TPolinom &q) // присваивание
{
	
	if ((&q) && (&q != this)) 
	{
		DelList();
		PTMonom Mon = new TMonom(0, -1);
		pHead->SetDatValue(Mon);
		int tempPosition = q.GetCurrentPos();
		for (q.Reset(); !q.IsListEnded(); q.GoNext()) 
		{
			Mon = q.GetMonom();
			InsLast(Mon->GetCopy());
		}
		q.SetCurrentPos(tempPosition);
	}
	return *this;
}

bool TPolinom::operator==(TPolinom &q)
{
	bool result = true;
	bool tempCheck = false;
	int tempPositionLeft = GetCurrentPos();
	int tempPositionRight = q.GetCurrentPos();
	if(q.GetListLength()==GetListLength())
	for (Reset();!IsListEnded();GoNext())
	{
		for (q.Reset(); !q.IsListEnded();q.GoNext())
		{		
			tempCheck = false;
			if (((PTMonom)GetDatValue())->operator==(*(PTMonom)(q.GetDatValue())))
			{
				tempCheck = true;
				break;
			}
		}
		if (!tempCheck)
			break;
	}
	result = tempCheck;
	if (q.GetListLength() == 0 && GetListLength() == 0)
		result = true;
	SetCurrentPos(tempPositionLeft);
	q.SetCurrentPos(tempPositionRight);
	return result;
}

double TPolinom::calculate(double x, double y, double z)
{
	double result = 0;
	if(ListLen!=0)
		for (Reset(); !IsListEnded(); GoNext())
			result += ((PTMonom)GetDatValue())->calculate(x, y, z);
	return result;
}
ostream & operator<<(ostream & os, TPolinom & q) {
	if (q.ListLen != 0)
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
			cout << (TMonom)(*q.GetMonom());
	else
		cout << "Polinom is empty";
	return os;
}

```

### Файл `test_tdatlist.cpp`

```c++

#pragma once

#include "gtest.h"
#include "DatList.h"

typedef struct ValueInt :TDatValue
{
	int Value;
	ValueInt() { Value = 0; }
	virtual TDatValue * GetCopy() { TDatValue *temp = new ValueInt; return temp; }
} *PValueInt;

//Initialization//////////////////////////////////////////////////////////////////
class TestDatList : public ::testing::Test
{
protected:
	virtual void SetUp()
	{
		// Testing only elem
		Val = new ValueInt();
		Val->Value = 100;

		// Testing ten elems
		ArrVal = new PValueInt[N];
		for (int i = 0; i < N; i++)
		{
			ArrVal[i] = new ValueInt();
			ArrVal[i]->Value = i;
		}
	}
	virtual void TearDown()
	{
		delete ArrVal;
	}
	const int N = 10;
	PValueInt Val;
	PValueInt *ArrVal;
	TDatList List;
};

TEST_F(TestDatList, new_list_has_null_size)
{
	// NO ACTIONS

	EXPECT_EQ(List.GetListLength(), 0);
}

TEST_F(TestDatList, reset_makes_current_position_equil_null)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.Reset();

	EXPECT_EQ(List.GetCurrentPos(), 0);
}

TEST_F(TestDatList, setcurrentpos_changes_the_current_position)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.SetCurrentPos(5);

	EXPECT_EQ(List.GetCurrentPos(), 5);
}

TEST_F(TestDatList, setcurrentpos_can_change_position_if_it_is_more_than_size_of_list)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);

	EXPECT_ANY_THROW(List.SetCurrentPos(12));
}

TEST_F(TestDatList, size_of_the_list_is_correct_after_inserting)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);

	EXPECT_EQ(List.GetListLength(), N);
}

TEST_F(TestDatList, size_of_the_list_is_correct_after_N_times_removing)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	for (int i = 0; i < N; i++)
		List.DelFirst();

	EXPECT_EQ(List.GetListLength(), 0);
}

TEST_F(TestDatList, size_of_the_list_is_correct_after_removing_of_all_list)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.DelList();

	EXPECT_EQ(List.GetListLength(), 0);
}

TEST_F(TestDatList, cant_delete_elem_if_size_equil_null)
{
	// NO ACTIONS

	EXPECT_ANY_THROW(List.DelFirst());
	EXPECT_ANY_THROW(List.DelCurrent());
}

TEST_F(TestDatList, list_is_ended_if_current_position_equil_length_of_list)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	List.Reset();
	List.SetCurrentPos(List.GetListLength());

	EXPECT_TRUE(List.IsListEnded());
}


TEST_F(TestDatList, can_add_an_item_to_the_end)
{
	for (int i = 0; i < N - 1; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8
	List.InsLast(Val);
	// 0 1 2 3 4 5 6 7 8 100
	List.SetCurrentPos(List.GetListLength()-1);

	EXPECT_EQ(((PValueInt)(List.GetDatValue()))->Value,Val->Value);
}

TEST_F(TestDatList, can_add_an_item_to_the_beginning)
{
	for (int i = 0; i < N - 1; i++)
		List.InsFirst(ArrVal[i]);
	// 8 7 6 5 4 3 2 1 0
	List.InsFirst(Val);
	// 100 8 7 6 5 4 3 2 1 0

	EXPECT_EQ(((PValueInt)(List.GetDatValue()))->Value, Val->Value);
}

TEST_F(TestDatList, can_add_an_item_to_the_current_position)
{
	for (int i = 0; i < N-1; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8
	// Current = 0
	List.SetCurrentPos(5);
	// Current = 5
	List.InsCurrent(Val);
	// 0 1 2 3 4 100 5 6 7 8

	EXPECT_EQ(((PValueInt)(List.GetDatValue()))->Value, Val->Value);
}

TEST_F(TestDatList, can_add_ten_elems_to_the_end)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8 9

	for (int i = 0; i < N; i++, List.GoNext())
		EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value);
}

TEST_F(TestDatList, can_add_ten_elems_to_the_beginnig)
{
	for (int i = 0; i < N; i++)
		List.InsFirst(ArrVal[i]);
	// 9 8 7 6 5 4 3 2 1 0

	for (int i = N - 1; i > 0; i--, List.GoNext())
		EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value);
}

TEST_F(TestDatList, can_add_ten_elems_to_the_current_pos)
{
	for (int i = 0; i < 5; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4
	// Current = 0
	List.GoNext();
	List.GoNext();
	// Current = 2
	List.InsCurrent(Val);
	// 0 1 100 2 3 4

	List.Reset();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[0]->Value);
	List.GoNext();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[1]->Value);
	List.GoNext();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, Val->Value);
	List.GoNext();
	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[2]->Value);
}

TEST_F(TestDatList, can_delete_elem_from_beginnig)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8 9
	List.DelFirst();
	// 1 2 3 4 5 6 7 8 9

	EXPECT_EQ(((PValueInt)(List.GetDatValue()))->Value, ArrVal[1]->Value);
	EXPECT_EQ(List.GetListLength(), N - 1);
}

TEST_F(TestDatList, can_delete_elem_from_the_current_pos)
{
	for (int i = 0; i < N; i++)
		List.InsLast(ArrVal[i]);
	// 0 1 2 3 4 5 6 7 8 9
	List.GoNext();
	List.DelCurrent();
	// 0 2 3 4 5 6 7 8 9

	EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[2]->Value);
	EXPECT_EQ(List.GetListLength(), N - 1);
}

```

![](http://i.imgur.com/oua4uvI.png)

### Файл `test_polinom.cpp`

```c++

#pragma once

#include "gtest.h"
#include "DatList.h"
#include "Polinom.h"

TEST(TPolinom, can_create_polinom_without_parametres)
{
	// NO ACTIONS

	EXPECT_NO_THROW(TPolinom Pol);
}

TEST(TPolinom, polinom_without_parametres_has_its_own_adress)
{
	TPolinom Pol;

	EXPECT_NE(&Pol,nullptr);
}

TEST(TPolinom, polinom_copied_by_constructer_is_equal_to_origin)
{
	const int size = 2;
	int mon[][2] = { { 5, 3 },{ 2, 4 } };
	TPolinom Pol1(mon, size);

	TPolinom Pol2 = Pol1;

	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, polinom_copied_by_constructer_has_its_own_adress)
{
	const int size = 2;
	int mon[][2] = { { 5, 3 },{ 2, 4 } };
	TPolinom Pol1(mon, size);

	TPolinom Pol2 = Pol1;

	EXPECT_NE(&Pol1,&Pol2);
}

TEST(TPolinom, can_compare_the_polynoms)
{
	const int size = 3;
	int mon[][2] = { { 1, 3 },{ 2, 4 },{ 2, 100 } };
	TPolinom Pol1(mon, size);
	TPolinom Pol2(mon, size);

	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, can_assign_polynoms_and_they_are_equal)
{
	// Arrange
	const int size = 2;
	int mon[][2] = { { 5, 3 },{ 2, 4 } };
	TPolinom Pol1(mon, size);
	TPolinom Pol2;

	Pol2 = Pol1;

	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, can_assign_many_polynoms_and_they_are_equal)
{
	const int size = 2;
	int mon[][2] = { { 5, 3 },{ 2, 4 } };
	TPolinom Pol1(mon, size);
	TPolinom Pol2,Pol3;

	Pol3=Pol2 = Pol1;
	bool _check = (Pol1 == Pol2) && (Pol2 == Pol3) && (Pol1 == Pol3);

	EXPECT_TRUE(_check);
}

TEST(TPolinom, can_add_up_simple_polynoms)
{
	const int size1 = 3;
	const int size2 = 4;
	int mon1[][2] = { { 5, 2 },{ 8, 3 },{ 9, 4 } };
	int mon2[][2] = { { 1, 1 },{ -8, 3 },{ 1, 4 },{ 2, 5 } };
	// 5z^2+8z^3+9z^4
	TPolinom Pol1(mon1, size1);
	// z-8z^3+z^4+2z^5
	TPolinom Pol2(mon2, size2);

	TPolinom Pol= Pol1 + Pol2;

	const int expected_size = 4;
	int expected_mon[][2] = { { 1, 1 },{ 5, 2 },{ 10, 4 },{ 2, 5 } };
	// z+5z^2+10z^4+2z^5
	TPolinom check_Pol(expected_mon, expected_size);

	EXPECT_TRUE(Pol == check_Pol);
}

TEST(TPolinom, can_create_polinom_from_one_monom)
{
	int mon[][2] = { { 1, 3 } };

	TPolinom A(mon, 1);

	TMonom res(1, 3);
	EXPECT_EQ(res, (TMonom)(*A.GetMonom()));
}

TEST(TPolinom, can_create_polinom_from_two_monoms)
{
	const int size = 2;
	int mon[][2] = { { 1, 3 },{ 2, 4 } };

	TPolinom Pol(mon, size);

	TMonom monoms[size];
	for (int i = 0; i < size; i++)
		monoms[i] = TMonom(mon[i][0], mon[i][1]);
	for (int i = 0; i < size; i++, Pol.GoNext())
		EXPECT_EQ(monoms[i], *Pol.GetMonom());
}

TEST(TPolinom, can_create_pol_from_many_monoms)
{
	const int size = 10;
	int mon[][2] = { { 1, 3 },{ 2, 4 },{ 2, 100 },{ 3, 110 },
	{ 5, 150 },{ 6, 302 },{ 3, 400 },{ 2, 500 },{ 7 ,800 },{ 2, 888 } };

	TPolinom Pol(mon, size);

	TMonom monoms[size];
	for (int i = 0; i < size; i++)
		monoms[i] = TMonom(mon[i][0], mon[i][1]);
	for (int i = 0; i < size; i++, Pol.GoNext())
		EXPECT_EQ(monoms[i], *Pol.GetMonom());
}

TEST(TPolinom, can_assign_empty_polynom)
{
	TPolinom Pol1;
	TPolinom Pol2;

	Pol2 = Pol1;

	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolinom, can_add_up_linear_polynoms)
{
	const int size = 1;
	int mon1[][2] = { { 2, 1 } };
	int mon2[][2] = { { 1, 1 } };
	// 2z
	TPolinom Pol1(mon1, size);
	// z
	TPolinom Pol2(mon2, size);

	TPolinom Pol = Pol1 + Pol2;

	const int expected_size = 1;
	int expected_mon[][2] = { { 3, 1 } };
	// z+2z^2
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_add_up_polynoms)
{
	const int size1 = 5;
	const int size2 = 4;
	int mon1[][2] = { { 5, 213 },{ 8, 321 },{ 10, 432 },{ -21, 500 },{ 10, 999 } };
	int mon2[][2] = { { 15, 0 },{ -8, 321 },{ 1, 500 },{ 20, 702 } };
	// 5x^2yz^3+8x^3y^2z+10x^4y^3z^2-21x^5+10x^9y^9z^9
	TPolinom Pol1(mon1, size1);
	// 15-8x^3y^2z+x^5+20x^7z^2
	TPolinom Pol2(mon2, size2);

	TPolinom Pol = Pol1 + Pol2;

	const int expected_size = 6;
	int expected_mon[][2] = { { 15, 0 },{ 5, 213 },{ 10, 432 },{ -20, 500 },{ 20, 702 },{ 10, 999 } };
	// 15+5x^2yz^3+10x^4y^3z^2-20x^5+20x^7z^2+10x^9y^9z^9
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_add_up_many_polynoms)
{
	const int size1 = 3;
	const int size2 = 4;
	const int size3 = 3;
	int mon1[][2] = { { 5, 2 },{ 8, 3 },{ 9, 4 } };
	int mon2[][2] = { { 1, 1 },{ -8, 3 },{ 1, 4 },{ 2, 5 } };
	int mon3[][2] = { { 10, 0 },{ 2, 3 },{ 8, 5 } };
	// 5z^2+8z^3+9z^4
	TPolinom Pol1(mon1, size1);
	// z-8z^3+z^4+2z^5
	TPolinom Pol2(mon2, size2);
	// 10+2z^3+8z^5
	TPolinom Pol3(mon3, size3);

	TPolinom Pol = Pol1 + Pol2 + Pol3;

	const int expected_size = 6;
	int expected_mon[][2] = { { 10, 0 },{ 1, 1 },{ 5, 2 },{ 2, 3 },{ 10, 4 },{ 10, 5 } };
	// z+5z^2+10z^4+2z^5
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolinom, can_calculate_the_polinom_with_different_monoms)
{
	int size = 3;
	int mon[][2] = { { 1, 1 },{ 2, 10 },{ 3, 100 } };
	TPolinom Pol(mon, size);

	EXPECT_EQ(Pol.calculate(1, 1, 1), 6);
}

TEST(TPolinom, can_calculate_the_polinom_with_equal_monoms_with_different_coeffs)
{
	int size = 3;
	int mon[][2] = { { 1, 111 },{ -2, 111 },{ 3, 111 } };
	TPolinom Pol(mon, size);

	EXPECT_EQ(Pol.calculate(1, 1, 1), 2);
}

TEST(TPolinom, can_calculate_the_polinom_with_different_and_equals_monoms)
{
	int size = 3;
	int mon[][2] = { { 1, 111 },{ -1, 111 },{ 2, 11 } };
	TPolinom Pol(mon, size);

	EXPECT_EQ(Pol.calculate(1, 1, 1), 2);
}

TEST(TPolinom, can_calculate_the_empty_polinom_and_it_is_equal_null)
{
	TPolinom Pol;

	EXPECT_EQ(Pol.calculate(1, 1, 1), 0);
}

```

![](http://i.imgur.com/FCjofUo.png)

## Демонстрация сложения и подсчета значения

```c++

#include "Polinom.h"

int main()
{
	cout << "\nTesting program. It will test method of outputing of polinom and method of calculating \nthe polinom's value\n\n";
	int mon1[][2] = { {-1,211},{ -4,302 } ,{2,222},{-1,101 } };
	int mon2[][2] = { { -4,211 },{ 4,302 } ,{ 2,22 },{ 0,101 },{1,0} };

	TPolinom pol1(mon1, 4);
	TPolinom pol2(mon2, 5);
	TPolinom pol3 = pol1 + pol2;

	cout << "The demonstration program will count values of polynoms at x=1, y=2, z=1\n\n";

	cout << "First polinom:\n" << pol1 <<" = "<<pol1.calculate(1,2,1)<< endl;
	cout << "\nSecond polinom:\n" << pol2 << " = " << pol2.calculate(1, 2, 1) << endl;
	cout << "\nSum of polinoms:\n" << pol3 << " = " << pol3.calculate(1, 2, 1) << endl<<endl;
	return 0;
}

```

![](http://i.imgur.com/ALOB859.png)

## Вывод:

 - в зависимости от выбора используемых структур хранения, сложность алгоритмов может возрастать или уменьшаться.


