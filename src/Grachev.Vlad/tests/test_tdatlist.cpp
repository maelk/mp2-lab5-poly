#include <gtest/gtest.h>
#include "DatList/DatList.h"
#include "DatList/DatList.cpp"
#include "Monom/Monom.h"

TEST(TDatList, can_create_list)
{
	ASSERT_NO_THROW(TDatList l1);
}

TEST(TDatList, new_list_is_empty)
{
	TDatList l1;
	ASSERT_TRUE(l1.IsEmpty());
}

TEST(TDatList, can_put_link_in_list)
{
	TDatList l1;
	PTMonom m1;
	m1 = new TMonom(4, 123);
	ASSERT_NO_THROW(l1.InsLast(m1));
}

TEST(TDatList, list_with_links_is_not_empty)
{
	TDatList l1;
	PTMonom m1, m2;
	m1 = new TMonom(4, 123);
	m2 = new TMonom(8, 456);
	l1.InsLast(m1);
	l1.InsLast(m2);
	ASSERT_TRUE((!l1.IsEmpty()) && (l1.GetListLength() == 2));
}

TEST(TDatList, can_get_current_position)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	EXPECT_EQ(l1.GetCurrentPos(), 0);
}

TEST(TDatList, can_set_current_position)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	EXPECT_EQ(l1.GetCurrentPos(), 4);
}

TEST(TDatList, can_go_next_link)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	l1.GoNext();
	EXPECT_EQ(l1.GetCurrentPos(), 5);
}

TEST(TDatList, can_reset_position)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	l1.Reset();
	EXPECT_EQ(l1.GetCurrentPos(), 0);
}

TEST(TDatList, ended_list_is_ended)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	l1.GoNext();
	EXPECT_TRUE(l1.IsListEnded());
}

TEST(TDatList, can_get_link_from_list)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(4);
	pVal = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(pVal->GetIndex() + pVal->GetCoeff(), 560);
}

TEST(TDatList, can_put_link_before_the_first)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 1; i < 6; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	pVal = new TMonom(6, 666);
	l1.InsFirst(pVal);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_put_link_after_the_last)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	pVal = new TMonom(6, 666);
	l1.InsLast(pVal);
	l1.SetCurrentPos(5);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_put_link_before_the_current)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(2);
	pVal = new TMonom(6, 666);
	l1.InsCurrent(pVal);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_delete_first_link)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	temp = (PTMonom)l1.GetDatValue();
	l1.DelFirst();
	pVal = (PTMonom)l1.GetDatValue();
	EXPECT_TRUE((temp->GetIndex() + temp->GetCoeff() != pVal->GetIndex() + pVal->GetCoeff()) && (l1.GetListLength() == 4));
}

TEST(TDatList, can_delete_current_link)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(3);
	l1.DelCurrent();
	l1.SetCurrentPos(3);
	pVal = (PTMonom)l1.GetDatValue();
	EXPECT_TRUE((pVal->GetIndex() + pVal->GetCoeff() == 448) && (l1.GetListLength() == 4));
}

TEST(TDatList, can_delete_list)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.DelList();
	EXPECT_TRUE((l1.IsEmpty()) && (l1.GetListLength() == 0) && (l1.GetDatValue() == nullptr));
}