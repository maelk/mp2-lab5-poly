#include <gtest/gtest.h>
#include "HeadRing/HeadRing.h"
#include "HeadRing/HeadRing.cpp"
#include "Monom/Monom.h"

TEST(THeadRing, can_create_headring_list)
{
	ASSERT_NO_THROW(THeadRing l1);
}

TEST(THeadRing, new_headring_list_is_empty)
{
	THeadRing l1;
	ASSERT_TRUE(l1.IsEmpty());
}

TEST(THeadRing, can_put_link_before_the_first)
{
	THeadRing l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 3; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	pVal = new TMonom(3, 333);
	l1.InsFirst(pVal);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 336);
}

TEST(THeadRing, can_delete_first_link)
{
	THeadRing l1;
	PTMonom pVal, temp;
	for (int i = 1; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.DelFirst();
	l1.SetCurrentPos(0);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_TRUE((temp->GetIndex() + temp->GetCoeff() == 224) && (l1.GetListLength() == 3));
}