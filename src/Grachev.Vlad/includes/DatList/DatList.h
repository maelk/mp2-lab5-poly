// DatList.h
// ������ ������ �������� �������

#ifndef _DATLIST_H_
#define _DATLIST_H_

#include "DataCom/tdatacom.h"
#include "DatLink/DatLink.h"

#define ListOK 0 // ������ ���
#define ListEmpty -101 // ������ ����
#define ListNoMem -102 // ��� ������
#define ListNoPos -103 // ��������� ��������� �������� ���������

enum TLinkPos {FIRST, CURRENT, LAST};

class DatList;
typedef TDatList *PTDatList;

class TDatList : public TDataCom{
protected:
	PTDatLink pFirst;    // ������ �����
	PTDatLink pLast;     // ��������� �����
	PTDatLink pCurrLink; // ������� �����
	PTDatLink pPrevLink; // ����� ����� �������
	PTDatLink pStop;     // �������� ���������, ����������� ����� ������ 
	int CurrPos;         // ����� �������� ����� (��������� �� 0)
	int ListLen;         // ���������� ������� � ������
protected:  // ������
	PTDatLink GetLink(PTDatValue pVal = nullptr, PTDatLink pLink = nullptr);
	void      DelLink(PTDatLink pLink);   // �������� �����
public:
	TDatList();
	~TDatList() { DelList(); }

	// ������
	PTDatValue GetDatValue(TLinkPos mode = CURRENT) const; // ��������
	virtual int IsEmpty()  const { return pFirst == pStop; } // �������� �������
	int GetListLength()    const { return ListLen; }       // ���������� �������

	// ���������
	int SetCurrentPos(int pos);          // ���������� ������� �����
	int GetCurrentPos(void) const;       // �������� ����� ���. �����
	virtual int Reset(void);             // ���������� ������� ������ �����
	virtual bool IsListEnded(void) const; // ������ �������� ?
	int GoNext(void);                    // ����� ������ �������� �����
										 // (=1 ����� ���������� GoNext ��� ���������� ����� ������)

	// ������� �������
	virtual void InsFirst(PTDatValue pVal = nullptr); // ����� ������
	virtual void InsLast(PTDatValue pVal = nullptr); // �������� ��������� 
	virtual void InsCurrent(PTDatValue pVal = nullptr); // ����� ������� 

	// �������� �������
	virtual void DelFirst(void);    // ������� ������ ����� 
	virtual void DelCurrent(void);    // ������� ������� ����� 
	virtual void DelList(void);    // ������� ���� ������
};

#endif