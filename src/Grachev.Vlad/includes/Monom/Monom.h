// Monom.h
// ������ ������ �����

#ifndef _MONOM_H_
#define _MONOM_H_

#include "DatValue/DatValue.h"

class TMonom;
typedef TMonom *PTMonom;

class TMonom : public TDatValue  {
protected:
	int Coeff; // ����������� ������
	int Index; // ������ (������� ��������)
public:
	TMonom(int cval = 1, int ival = 0) {
		Coeff = cval; Index = ival;
	};
	void SetCoeff(int cval) { Coeff = cval; }
	int  GetCoeff(void)     { return Coeff; }
	void SetIndex(int ival) { Index = ival; }
	int  GetIndex(void)     { return Index; }
	virtual TDatValue * GetCopy() // ���������� �����
	{
		TDatValue * temp = new TMonom(Coeff, Index);
		return temp;
	}
	TMonom& operator=(const TMonom &tm) {
		Coeff = tm.Coeff; Index = tm.Index;
		return *this;
	}
	int operator==(const TMonom &tm) {
		return (Coeff == tm.Coeff) && (Index == tm.Index);
	}
	int operator<(const TMonom &tm) {
		return Index<tm.Index;
	}
	friend class TPolinom;
};
#endif