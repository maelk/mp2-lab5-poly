#ifndef __POLINOM_H__
#define __POLINOM_H__

#include "HeadRing.h"
#include "Monom.h"

#include <iostream>
using namespace std;

class TPolinom : public THeadRing {
public:
	TPolinom(int monoms[][2] = NULL, int km = 0); // конструктор

	TPolinom(TPolinom &q);      // конструктор копирования
	PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }
	TPolinom & operator+(TPolinom &q); // сложение полиномов
	TPolinom & operator=(TPolinom &q); // присваивание
	bool operator==(TPolinom &q); // сравнение полиномов
	bool operator!=(TPolinom &q); // сравнение полиномов
	double CalculatePoly(int x = 0, int y = 0, int z = 0);
	friend ostream& operator<<(ostream &os, TPolinom &q);// печать полинома
};

#endif