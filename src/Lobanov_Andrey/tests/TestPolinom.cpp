#include "Polinom.h"
#include <gtest.h>

TEST(Polinom, Can_Create_Polinoms)
{
	ASSERT_NO_THROW(TPolinom poly);
}

TEST(Polinom, Can_Create_Polinoms_With_Parameters)
{
	int monoms[][2] = { { 2,123 },{ -3,332 },{ 91,401 }, {5,0} };
	TPolinom poly(monoms, 4);
	ASSERT_NO_THROW(TPolinom poly);
}

TEST(Polinom, Compare_Work_Correctly)
{
	int monoms[][2] = { { 2,123 },{ -3,332 },{ 91,401 },{ 5,0 } };
	TPolinom poly(monoms, 4);
	TPolinom poly_1(monoms, 4);
	EXPECT_TRUE(poly == poly_1);
}

TEST(Polinom, Check_For_Unequal_Work_Correctly)
{
	int monoms[][2] = { { 2,123 },{ -3,332 },{ 91,401 },{ 5,0 } };
	int monoms_1[][2] = { { 3,14}, {15,926} };
	TPolinom poly(monoms, 4);
	TPolinom poly_1(monoms_1, 2);
	EXPECT_TRUE(poly != poly_1);
}

TEST(Polinom, Can_Create_Polinoms_Equal_Origin)
{
	int monoms[][2] = { { 2,123 },{ -3,332 },{ 91,401 },{ 5,0 } };
	TPolinom poly(monoms, 4);
	TPolinom poly_1 = poly;
	EXPECT_TRUE(poly == poly_1);
}

TEST(Polinom, Can_Copied_Polinoms_Equal_Origin)
{
	int monoms[][2] = { { 2,123 },{ -3,332 },{ 91,401 },{ 5,0 } };
	TPolinom poly(monoms, 4);
	TPolinom poly_1(poly);
	EXPECT_TRUE(poly == poly_1);
}

TEST(Polinom, Can_Sum_Polinoms)
{
	int monoms1[][2] = { { 197, 864 },{ 116, 236 },{ 13, 13 },{ 11, 124 } };
	int monoms2[][2] = { { -197, 864 },{ -13, 13 } };
	int result[][2] = { { 116, 236 },{ 11, 124 } };
	TPolinom poly1(monoms1, 4), poly2(monoms2, 2), res(result, 2), sum;
	sum = poly1 + poly2;
	EXPECT_TRUE(sum == res);
}

TEST(Polinom, Can_Multiple_Sum_Polinoms)
{
	int monoms1[][2] = { { 234, 479 }, { 23, 389 }, { 23, 24 }, { 1, 12 } };
	int monoms2[][2] = { { -232, 479 }, { -24, 24 }, {2,13} };
	int monoms3[][2] = { { -2, 479 }, { 1, 24 }, { -2,13 } };
	int result[][2] = { { 23, 389 }, { 1, 12 } };
	TPolinom poly1(monoms1, 4), poly2(monoms2, 3), poly3(monoms3,3), res(result, 2), sum;
	sum = poly1 + poly2 + poly3;
	EXPECT_TRUE(sum == res);	
}

TEST(Polinom, Can_Calculate_Polinom)
{
	int mon[][2] = { { 2,531 },{ -3,332 },{ 91,301 } };
	
	TPolinom poly(mon, 3);
	EXPECT_EQ(90 ,poly.CalculatePoly(1,1,1));
}