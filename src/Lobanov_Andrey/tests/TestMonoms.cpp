#include <gtest.h>
#include "Monom.h"

TEST(Monom, Can_Create_Monom)
{
	ASSERT_NO_THROW(TMonom m);
}

TEST(Monom, Can_Create_Monom_With_Parameters)
{
	ASSERT_NO_THROW(TMonom m(10, 253));
}

TEST(Monom, Can_Set_Monom_Coeff)
{
	TMonom m;
	ASSERT_NO_THROW(m.SetCoeff(15));
}

TEST(Monom, Can_Get_Monom_Coeff)
{
	TMonom m(10, 20);
	ASSERT_NO_THROW(m.GetCoeff());
}

TEST(Monom, Get_Monom_Coeff_Work_Correctly)
{
	TMonom m(10, 253);
	EXPECT_EQ(10, m.GetCoeff());
}


TEST(Monom, Set_Monom_Coeff_Work_Correctly)
{
	TMonom m;
	m.SetCoeff(15);
	EXPECT_EQ(15, m.GetCoeff());
}

TEST(Monom, Can_Set_Monom_Index)
{
	TMonom m;
	ASSERT_NO_THROW(m.SetIndex(20));
}

TEST(Monom, Can_Get_Monom_Index)
{
	TMonom m(10, 20);
	ASSERT_NO_THROW(m.GetIndex());
}

TEST(Monom, Get_Monom_Index_Work_Correctly)
{
	TMonom m(10, 13);
	EXPECT_EQ(13, m.GetIndex());
}

TEST(Monom, Set_Monom_Index_Work_Correctly)
{
	TMonom m;
	m.SetIndex(20);
	EXPECT_EQ(20, m.GetIndex());
}

TEST(Monom, Can_Create_Assign_Monoms)
{
	TMonom m1(10, 30);
	TMonom m2 = m1;
	EXPECT_TRUE((m1.GetCoeff() == m2.GetCoeff()) && (m1.GetIndex() == m2.GetIndex()));
}

TEST(Monom, Compare_Monoms_Work_Correctly)
{
	TMonom m1(13, 129), m2(13, 129);
	EXPECT_TRUE(m1 == m2);
}

TEST(Monom, Compare_Unequal_Monoms_Work_Correctly)
{
	TMonom m1(10, 141), m2(4, 198);
	EXPECT_TRUE(m1 < m2);
}