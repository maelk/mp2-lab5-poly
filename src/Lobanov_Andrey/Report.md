﻿# Лабораторная работа №5. Полиномы #


## Цели и задачи

В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное представление полиномов и выполнение следующих операций над ними:

*	ввод полинома;
*	организация хранения полинома;
*	удаление введенного ранее полинома;
*	копирование полинома;
*	сложение двух полиномов;
* 	вычисление значения полинома при заданных значениях переменных;
*	вывод.

В качестве структуры хранения используются циклические списки с заголовком. В числе операций над списками реализованы следующие действия:

*	поддержка понятия текущего звена;
*	вставка звеньев в начало, после текущей позиции и в конец списков;
*	удаление звеньев в начале и в текущей позиции списков;
*	организация последовательного доступа к звеньям списка (итератор).

### Условия и ограничения

При выполнении лабораторной работы использовались следующие основные предположения:

1.	Разработка структуры хранения ориентирована на представление полиномов от трех неизвестных.
2.	Степени переменных полиномов не могут превышать значения 9.
3.	Число мономов в полиномах существенно меньше максимально возможного количества (тем самым, в структуре хранения должны находиться только мономы с ненулевыми коэффициентами).

### Структуры хранения полиномов

Для представления полиномов могут быть выбраны различные структуры хранения. Критериями выбора структуры хранения являются размер требуемой памяти и сложность (трудоемкость) реализации операций над полиномами.

Возможный вариант структуры хранения – использование массивов (в случае полиномов от трех переменных – трехмерная матрица коэффициентов полинома). Такой способ обеспечивает простую реализацию операций над полиномами, но он не эффективен в части объема требуемой памяти. Так, при сделанных допущениях для хранения одного полинома в массиве потребуется порядка 8000 байт памяти – при этом в памяти будут храниться в основном параметры мономов с нулевыми коэффициентами.

Разработка более эффективной структуры хранения должна быть выполнена с учетом следующих рекомендаций:

 - в структуре хранения должны храниться данные только для мономов с ненулевыми коэффициентами; 
 - порядок размещения данных в структуре хранения должен обеспечивать возможность быстрого поиска мономов с заданными свойствами (например, для приведения подобных).

Так как степени переменных мономов ограничены и принимают значения от 0 до 9, то можно использовать ОДНО число, которое будет принимать значения от 0 до 999. Причем, если A, B и C - степени, то это число равно ABC = A*100+B*10+C. 
	
Данное соответствие является взаимно-однозначным. Обратное соответствие определяется при помощи выражений
A=E(ABC%100),  B=E(ABC-A*100)%10,  C=ABC-A*100-B*10.

Наиболее эффективным способом организации структуры хранения полиномов являются линейный (односвязный) список. Тем самым, в рамках лабораторной работы появляется подзадача – разработка структуры хранения в виде линейных списков. 

Для работы со списками должны быть реализованы следующие операции:

 - методы получения параметров состояния списка (проверка на пустоту, получение текущего количества звеньев);
 - метод доступа к значению первого, текущего или последнего звена;
 - методы навигации по списку (итератор);
 - методы вставки перед первым, после текущего и последнего звеньев;
 - методы удаления первого и текущего звена.

Для работы с полиномами должны быть реализованы следующие операции:

 - конструкторы инициализации и копирования;
 - метод присваивания;
 - метод сложения полиномов.

### Общая структура классов:

![](https://pp.userapi.com/c837629/v837629927/37fcc/xZ0piprxliE.jpg)

* TDatValue - абстрактный класс объектов-значений списка
* TMonom - класс мономов
* TRootLink - базовый класс для звеньев
* TDatLink - класс для звеньев (элементов) списка с указателем на объект-значение
* TDatList - класс линейных списков
* THeadRing - класс циклических списков с заголовком
* TPolinom - класс полиномов

### План работы
1.      Разработка структуры хранения списков.
2.      Разработка структуры хранения полиномов.
3.      Проверка работоспособности написанных классов с помощью Google Test Framework.

## Выполнение работы

### 1. Реализация классов.
 
 __Реализация класса `TDatValue`:__

```c++

#ifndef __DATVALUE_H__
#define __DATVALUE_H__

#include <iostream>

class TDatValue {
public:
	virtual TDatValue * GetCopy() = 0; // создание копии
	~TDatValue() {}
};
typedef TDatValue *PTDatValue;

#endif
```
 __Реализация класса `TMonom`:__

```c++
#include "DatValue.h"
#include <iostream>
using namespace std;

class TMonom;
typedef TMonom* PTMonom;

class TMonom : public TDatValue {
protected:
	int Coeff; // коэффициент монома
	int Index; // индекс (свертка степеней)
public:
	TMonom(int cval = 1, int ival = 0) {
		Coeff = cval; Index = ival;
	};
	virtual TDatValue * GetCopy() { return new TMonom(Coeff, Index); } // изготовить копию
	void SetCoeff(int cval) { Coeff = cval; }
	int  GetCoeff(void) { return Coeff; }
	void SetIndex(int ival) { Index = ival; }
	int  GetIndex(void) { return Index; }
	TMonom& operator=(const TMonom &tm) {
		Coeff = tm.Coeff; Index = tm.Index;
		return *this;
	}
	int operator==(const TMonom &tm) {
		return (Coeff == tm.Coeff) && (Index == tm.Index);
	}
	int operator<(const TMonom &tm) {
		return Index<tm.Index;
	}
	friend ostream& operator<<(ostream &os, TMonom &tm) {
		os << tm.Coeff << " " << tm.Index;
		return os;
	}
	friend class TPolinom;
};

#endif
```
 __Реализация класса `TRootLink`:__

```c++
#ifndef __ROOTLINK_H__
#define __ROOTLINK_H__

#include "DatValue.h"

class TRootLink;
typedef TRootLink *PTRootLink;

class TRootLink {
protected:
	PTRootLink  pNext;  // указатель на следующее звено
public:
	TRootLink(PTRootLink pN = NULL) { pNext = pN; }
	PTRootLink  GetNextLink() { return  pNext; }
	void SetNextLink(PTRootLink  pLink) { pNext = pLink; }
	void InsNextLink(PTRootLink  pLink)
	{
		PTRootLink p = pNext;
		pNext = pLink;
		if (pLink != NULL) pLink->pNext = p;
	}
	virtual void       SetDatValue(PTDatValue pVal) = 0;
	virtual PTDatValue GetDatValue() = 0;

	friend class TDatList;
};

#endif
```
 __Реализация класса `TDatLink`:__

```c++
#ifndef __DATLINK_H__
#define __DATLINK_H__

#include "RootLink.h"

class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink {
protected:
	PTDatValue pValue;  // указатель на объект значения
public:
	TDatLink(PTDatValue pVal = NULL, PTRootLink pN = NULL) : TRootLink(pN)
	{
		pValue = pVal;
	}
	void       SetDatValue(PTDatValue pVal) { pValue = pVal; }
	PTDatValue GetDatValue() { return  pValue; }
	PTDatLink  GetNextDatLink() { return  (PTDatLink)pNext; }
	friend class TDatList;
};

#endif
}
```

 __Реализация класса `TDatList`:__
 
```c++
// DatList.h

#ifndef __DATLIST_H__
#define __DATLIST_H__

#include "tdatacom.h"
#include "DatLink.h"

#define ListOK 0 // ошибок нет
#define ListEmpty -101 // список пуст
#define ListNoMem -102 // нет памяти
#define ListNoPos -103 // ошибочное положение текущего указателя

enum TLinkPos { FIRST, CURRENT, LAST };

class DatList;
typedef TDatList *PTDatList;

class TDatList : public TDataCom {
protected:
	PTDatLink pFirst;    // первое звено
	PTDatLink pLast;     // последнее звено
	PTDatLink pCurrLink; // текущее звено
	PTDatLink pPrevLink; // звено перед текущим
	PTDatLink pStop;     // значение указателя, означающего конец списка 
	int CurrPos;         // номер текущего звена (нумерация от 0)
	int ListLen;         // количество звеньев в списке
protected:  // методы
	PTDatLink GetLink(PTDatValue pVal = NULL, PTDatLink pLink = NULL);
	void      DelLink(PTDatLink pLink);   // удаление звена
public:
	TDatList();
	~TDatList() { DelList(); }

	// доступ
	PTDatValue GetDatValue(TLinkPos mode = CURRENT) const; // значение
	virtual int IsEmpty()  const { return pFirst == pStop; } // проверка пустоты
	int GetListLength()    const { return ListLen; }       // количество звеньев

														   // навигация
	int SetCurrentPos(int pos);          // установить текущее звено
	int GetCurrentPos(void) const;       // получить номер тек. звена
	virtual int Reset(void);             // установить текущим первое звено
	virtual bool IsListEnded(void) const; // список завершен ?
	int GoNext(void);                    // сдвиг вправо текущего звена
										 // (=1 после применения GoNext для последнего звена списка)

										 // вставка звеньев
	virtual void InsFirst(PTDatValue pVal = NULL); // перед первым
	virtual void InsLast(PTDatValue pVal = NULL); // вставить последним 
	virtual void InsCurrent(PTDatValue pVal = NULL); // перед текущим 

													 // удаление звеньев
	virtual void DelFirst(void);    // удалить первое звено 
	virtual void DelCurrent(void);    // удалить текущее звено 
	virtual void DelList(void);    // удалить весь список
};

#endif

// DatList.cpp

#include "DatList.h"

TDatList::TDatList() {
	pFirst = pLast = pStop = NULL;
	ListLen = 0;
	Reset();
}

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
	PTDatLink temp = new TDatLink(pVal, pLink);  //выделение звена
	if (temp == NULL)
		SetRetCode(ListNoMem);
	else SetRetCode(ListOK);
	return temp;
}

void TDatList::DelLink(PTDatLink pLink)  // удаление звена
{
	if (pLink != NULL) {
		if (pLink->pValue != NULL)
			delete pLink->pValue;
		delete pLink;
	}
	SetRetCode(ListOK);
}

// методы доступа
PTDatValue TDatList::GetDatValue(TLinkPos mode) const  //значение
{
	PTDatLink temp;
	switch (mode) {
	case FIRST: temp = pFirst;
		break;
	case LAST: temp = pLast;
		break;
	default: temp = pCurrLink;
		break;
	}
	return (temp == NULL) ? NULL : temp->pValue;
}

// методы навигации 
int TDatList::SetCurrentPos(int pos)  // установить текущеее звено
{
	Reset();
	for (int i = 0; i < pos; i++, GoNext())
		SetRetCode(ListOK);
	return RetCode;
}
int TDatList::GetCurrentPos(void) const  // получить норер текущего звена
{
	return CurrPos;
}

int TDatList::Reset(void)  // установить на начало списка
{
	pPrevLink = pStop;
	if (IsEmpty()) {
		pCurrLink = pStop;
		CurrPos = -1;
		SetRetCode(ListEmpty);
	}
	else {
		pCurrLink = pFirst;
		CurrPos = 0;
		SetRetCode(ListOK);
	}
	return RetCode;
}

int TDatList::GoNext(void)  // сдвиг вправо текущего звена
{
	if (pCurrLink == pStop)
		SetRetCode(ListNoPos);
	else {
		SetRetCode(ListOK);
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
	}
	return RetCode;
}

bool TDatList::IsListEnded(void) const  // список завершён?
{
	return pCurrLink == pStop;
}

// вставка звеньев
void TDatList::InsFirst(PTDatValue pVal)  // перед первым
{
	PTDatLink temp = GetLink(pVal, pFirst);
	if (temp == NULL)
		SetRetCode(ListNoMem);
	else {
		pFirst = temp;
		ListLen++;
		// проверка пустоты списка перед вставкой
		if (ListLen == 1) {
			pLast = temp;
			Reset();
		}
		// корректировка текущей позиции - отличие обработки для начала списка
		else if (CurrPos == 0)
			pCurrLink = temp;
		else
			CurrPos++;
		SetRetCode(ListOK);
	}
}

void TDatList::InsLast(PTDatValue pVal)  // последним
{
	PTDatLink temp = GetLink(pVal, pStop);
	if (temp == NULL)
		SetRetCode(ListNoMem);
	else {
		if (pLast != NULL)
			pLast->SetNextLink(temp);
		pLast = temp;
		ListLen++;
		// проверка пустоты списка перед вставкой
		if (ListLen == 1) {
			pFirst = temp;
			Reset();
		}
		// корректировка текущей позиции - отличие при pCurrLink за концом списка
		if (IsListEnded())
			pCurrLink = temp;
		SetRetCode(ListOK);
	}
}

void TDatList::InsCurrent(PTDatValue pVal)  // перед текущим
{
	if (IsEmpty() || (pCurrLink == pFirst))
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else if (pPrevLink == pStop)
		SetRetCode(ListNoMem);
	else {
		PTDatLink temp = GetLink(pVal, pCurrLink);
		if (temp == NULL)
			SetRetCode(ListNoMem);
		else {
			pCurrLink = temp;
			pPrevLink->SetNextLink(temp);
			ListLen++;
			SetRetCode(ListOK);
		}
	}
}

// методы удаления звеньев
void TDatList::DelFirst(void) //  удалить первое звено
{
	if (IsEmpty())
		SetRetCode(ListEmpty);
	else {
		PTDatLink temp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(temp);
		ListLen--;
		if (IsEmpty()) {
			pLast = pStop;
			Reset();
		}
		// корректировка текущей позиции - отличие обработки для начала списка
		else if (CurrPos == 0)
			pCurrLink = pFirst;
		else if (CurrPos == 1)
			pPrevLink = pStop;
		if (CurrPos > 0)
			CurrPos--;
		SetRetCode(ListOK);
	}
}

void TDatList::DelCurrent(void) // удалить текущее звено
{
	if (pCurrLink == pStop)
		SetRetCode(ListNoPos);
	else if (pCurrLink == pFirst)
		DelFirst();
	else
	{
		PTDatLink temp = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		pPrevLink->SetNextLink(pCurrLink);
		DelLink(temp);
		ListLen--;
		// обработка ситуации удаления последнего звена
		if (pCurrLink == pLast)
		{
			pLast = pPrevLink;
			pCurrLink = pStop;
		}
		SetRetCode(ListOK);
	}
}

void TDatList::DelList(void) // удалить весь список
{
	while (!IsEmpty())
		DelFirst();
	pFirst = pLast = pPrevLink = pCurrLink = pStop;
	CurrPos = -1;
}

```
 __Реализация класса `THeadRing`:__
 
```c++
// HeadRing.h
#include "HeadRing.h"

#ifndef __HEADRING_H__
#define __HEADRING_H__

#include "DatList.h"

class THeadRing : public TDatList {
protected:
	PTDatLink pHead;     // заголовок, pFirst - будет следующим за pHead
public:
	THeadRing();
	~THeadRing();
	// вставка звеньев
	virtual void InsFirst(PTDatValue pVal = NULL); // после заголовка
												   // удаление звеньев
	virtual void DelFirst(void);                 // удалить первое звено
};
#endif

// HeadRing.cpp

#include "HeadRing.h"

THeadRing::THeadRing() : TDatList()
{
	InsLast();
	pHead = pFirst;
	ListLen = 0;
	pStop = pHead;
	Reset();
	pFirst->SetNextLink(pFirst);
}

THeadRing::~THeadRing()
{
	TDatList::~TDatList();
	DelLink(pHead);
	pHead = NULL;
}

void THeadRing::InsFirst(PTDatValue pVal) // вставить после заголовка
{
	TDatList::InsFirst(pVal);
	if (RetCode == DataOK)
		pHead->SetNextLink(pFirst);
}

void THeadRing::DelFirst(void) // удалить первое звено
{
	TDatList::DelFirst();
	pHead->SetNextLink(pFirst);
}
```

 __Реализация класса `TPolinom`:__

```c++
// Polinom.h
#ifndef __POLINOM_H__
#define __POLINOM_H__

#include "HeadRing.h"
#include "Monom.h"

#include <iostream>
using namespace std;

class TPolinom : public THeadRing {
public:
	TPolinom(int monoms[][2] = NULL, int km = 0); // конструктор

	TPolinom(TPolinom &q);      // конструктор копирования
	PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }
	TPolinom & operator+(TPolinom &q); // сложение полиномов
	TPolinom & operator=(TPolinom &q); // присваивание
	bool operator==(TPolinom &q); // сравнение полиномов
	bool operator!=(TPolinom &q); // сравнение полиномов
	double CalculatePoly(int x = 0, int y = 0, int z = 0);
	friend ostream& operator<<(ostream &os, TPolinom &q);// печать полинома
};

#endif

// Polinom.cpp

#include "Polinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom pMonom = new TMonom(0, -1);
	pHead->SetDatValue(pMonom);
	for (int i = 0; i < km; i++)
	{
		pMonom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(pMonom);
	}
}

TPolinom & TPolinom::operator+(TPolinom &q) // сложение полиномов
{
	PTMonom pm, qm, tm;
	Reset();
	q.Reset();
	while (1)
	{
		pm = GetMonom();
		qm = q.GetMonom();
		if (pm->Index < qm->Index)
		{
			// степени монома pm меньше степеней монома qm => добавление монома qm  в полином p
			tm = new TMonom(qm->Coeff, qm->Index);
			InsCurrent(tm);
			q.GoNext();
		}
		else if (pm->Index > qm->Index)
			GoNext();
		else // индексы мономов равны
		{
			if (pm->Index == -1) // звенья не должны быть заголовочными
				break;
			pm->Coeff += qm->Coeff;
			if (pm->Coeff != 0)
			{
				GoNext();
				q.GoNext();
			}
			else // удаление монома с нулевым коэффициентом
			{
				DelCurrent();
				q.GoNext();
			}
		}
	}
	return *this;
}

TPolinom::TPolinom(TPolinom &q) // конструктор копирования
{
	PTMonom pMonom = new TMonom(0, -1);
	pHead->SetDatValue(pMonom);
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		pMonom = q.GetMonom();
		InsLast(pMonom->GetCopy());
	}
	q.Reset();
}

TPolinom & TPolinom::operator=(TPolinom &q) // присваивание
{
	if (this != &q)
	{
		PTMonom pMonom;
		DelList();
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			pMonom = q.GetMonom();
			InsLast(pMonom->GetCopy());
		}
	}
	return *this;
}

ostream& operator<<(ostream &os, TPolinom &q)
{
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
		cout << *q.GetMonom() << endl;
	return os;
}

bool TPolinom::operator==(TPolinom &q) // сравнение полиномов
{
	if (GetListLength() != q.GetListLength())
		return false;
	else
	{
		PTMonom Mon1, Mon2;
		Reset(); q.Reset();
		while (!IsListEnded())
		{
			Mon1 = GetMonom();
			Mon2 = GetMonom();
			if (*Mon1 == *Mon2)
			{
				GoNext(); q.GoNext();
			}
			else
				return false;
		}
		return true;
	}
}

bool TPolinom::operator!=(TPolinom &q) // сравнение полиномов
{
	return !(*this == q);
}

double TPolinom::CalculatePoly(int x, int y, int z)
{
	double res = 0;
	PTMonom mon;
	int indx, indy, indz;
	if (ListLen)
	{
		for (Reset(); !IsListEnded(); GoNext())
		{
			mon = GetMonom();
			indx = mon->Index / 100;
			indy = (mon->Index % 100) / 10;
			indz = mon->Index % 10;
			res += mon->Coeff*pow(x, indx)*pow(y, indy)*pow(z, indz);
		}
	}
	return res;
}
```

### 2. Пример использования класса `TPolinom`:__

```c++
// DemonstrationProg.cpp

#include "Polinom.h"
#include <iostream>

using namespace std;

void main()
{
	int ms1[][2] = { { 1,123 },{ 2,332 },{ 3,151 } };
	int mn1 = sizeof(ms1) / (2 * sizeof(int));
	TPolinom p(ms1, mn1);
	cout << "polinom #1" << endl << p << endl;
	int ms2[][2] = { { 1,123 },{ 3,332 },{ 3,151 }, {4,514} };
	int mn2 = sizeof(ms2) / (2 * sizeof(int));
	TPolinom q(ms2, mn2);
	cout << "polinom #2" << endl << q << endl;

	if (p == q)
		cout << "polinom #1 = polinom #2" << endl;
	else
		cout << "polinom #1 != polinom #2" << endl;
	TPolinom r = p + q;
	cout << "p + q = " << endl << r << endl;
	cout << "result r = p + q, where x = 1, y = 1, z = 2: " << endl << r.CalculatePoly(1, 1, 2) << endl;
	system("pause");
}
```

__Результат работы тестового приложения:__

![](https://pp.userapi.com/c837629/v837629927/37fbc/Aqt4PIMOyPY.jpg)

### 3. Проверка работоспособности при помощи Google Test Framework

__Тесты для класса `TMonom`:__
 
```c++
#include <gtest.h>
#include "Monom.h"

TEST(Monom, Can_Create_Monom)
{
	ASSERT_NO_THROW(TMonom m);
}

TEST(Monom, Can_Create_Monom_With_Parameters)
{
	ASSERT_NO_THROW(TMonom m(10, 253));
}

TEST(Monom, Can_Set_Monom_Coeff)
{
	TMonom m;
	ASSERT_NO_THROW(m.SetCoeff(15));
}

TEST(Monom, Can_Get_Monom_Coeff)
{
	TMonom m(10, 20);
	ASSERT_NO_THROW(m.GetCoeff());
}

TEST(Monom, Get_Monom_Coeff_Work_Correctly)
{
	TMonom m(10, 253);
	EXPECT_EQ(10, m.GetCoeff());
}


TEST(Monom, Set_Monom_Coeff_Work_Correctly)
{
	TMonom m;
	m.SetCoeff(15);
	EXPECT_EQ(15, m.GetCoeff());
}

TEST(Monom, Can_Set_Monom_Index)
{
	TMonom m;
	ASSERT_NO_THROW(m.SetIndex(20));
}

TEST(Monom, Can_Get_Monom_Index)
{
	TMonom m(10, 20);
	ASSERT_NO_THROW(m.GetIndex());
}

TEST(Monom, Get_Monom_Index_Work_Correctly)
{
	TMonom m(10, 13);
	EXPECT_EQ(13, m.GetIndex());
}

TEST(Monom, Set_Monom_Index_Work_Correctly)
{
	TMonom m;
	m.SetIndex(20);
	EXPECT_EQ(20, m.GetIndex());
}

TEST(Monom, Can_Create_Assign_Monoms)
{
	TMonom m1(10, 30);
	TMonom m2 = m1;
	EXPECT_TRUE((m1.GetCoeff() == m2.GetCoeff()) && (m1.GetIndex() == m2.GetIndex()));
}

TEST(Monom, Compare_Monoms_Work_Correctly)
{
	TMonom m1(13, 129), m2(13, 129);
	EXPECT_TRUE(m1 == m2);
}

TEST(Monom, Compare_Unequal_Monoms_Work_Correctly)
{
	TMonom m1(10, 141), m2(4, 198);
	EXPECT_TRUE(m1 < m2);
}
```
__Тесты для класса `TPolinom`:__

```c++
#include "Polinom.h"
#include <gtest.h>

TEST(Polinom, Can_Create_Polinoms)
{
	ASSERT_NO_THROW(TPolinom poly);
}

TEST(Polinom, Can_Create_Polinoms_With_Parameters)
{
	int monoms[][2] = { { 2,123 },{ -3,332 },{ 91,401 }, {5,0} };
	TPolinom poly(monoms, 4);
	ASSERT_NO_THROW(TPolinom poly);
}

TEST(Polinom, Compare_Work_Correctly)
{
	int monoms[][2] = { { 2,123 },{ -3,332 },{ 91,401 },{ 5,0 } };
	TPolinom poly(monoms, 4);
	TPolinom poly_1(monoms, 4);
	EXPECT_TRUE(poly == poly_1);
}

TEST(Polinom, Check_For_Unequal_Work_Correctly)
{
	int monoms[][2] = { { 2,123 },{ -3,332 },{ 91,401 },{ 5,0 } };
	int monoms_1[][2] = { { 3,14}, {15,926} };
	TPolinom poly(monoms, 4);
	TPolinom poly_1(monoms_1, 2);
	EXPECT_TRUE(poly != poly_1);
}

TEST(Polinom, Can_Create_Polinoms_Equal_Origin)
{
	int monoms[][2] = { { 2,123 },{ -3,332 },{ 91,401 },{ 5,0 } };
	TPolinom poly(monoms, 4);
	TPolinom poly_1 = poly;
	EXPECT_TRUE(poly == poly_1);
}

TEST(Polinom, Can_Copied_Polinoms_Equal_Origin)
{
	int monoms[][2] = { { 2,123 },{ -3,332 },{ 91,401 },{ 5,0 } };
	TPolinom poly(monoms, 4);
	TPolinom poly_1(poly);
	EXPECT_TRUE(poly == poly_1);
}

TEST(Polinom, Can_Sum_Polinoms)
{
	int monoms1[][2] = { { 197, 864 },{ 116, 236 },{ 13, 13 },{ 11, 124 } };
	int monoms2[][2] = { { -197, 864 },{ -13, 13 } };
	int result[][2] = { { 116, 236 },{ 11, 124 } };
	TPolinom poly1(monoms1, 4), poly2(monoms2, 2), res(result, 2), sum;
	sum = poly1 + poly2;
	EXPECT_TRUE(sum == res);
}

TEST(Polinom, Can_Multiple_Sum_Polinoms)
{
	int monoms1[][2] = { { 234, 479 }, { 23, 389 }, { 23, 24 }, { 1, 12 } };
	int monoms2[][2] = { { -232, 479 }, { -24, 24 }, {2,13} };
	int monoms3[][2] = { { -2, 479 }, { 1, 24 }, { -2,13 } };
	int result[][2] = { { 23, 389 }, { 1, 12 } };
	TPolinom poly1(monoms1, 4), poly2(monoms2, 3), poly3(monoms3,3), res(result, 2), sum;
	sum = poly1 + poly2 + poly3;
	EXPECT_TRUE(sum == res);	
}

TEST(Polinom, Can_Calculate_Polinom)
{
	int mon[][2] = { { 2,531 },{ -3,332 },{ 91,301 } };
	
	TPolinom poly(mon, 3);
	EXPECT_EQ(90 ,poly.CalculatePoly(1,1,1));
}
```
__Результат выполнения тестов:__

![](https://pp.userapi.com/c837629/v837629927/37fc4/QeBssRt2RhM.jpg)

### 4. Вывод  

- В ходе выполнения данной работы были получены навыки работы с принципиально новой динамической структуры данных Список, на основе которой был создан класс Полином в качестве звеньев которого выступил класс Моном.
- В очердной раз использовался один из фундаментальных принципов ООП - наследование, которое позволило существенно упростить работу и повысить надёжность программного кода.
- Корректность работы написанной системы была протестирована при помощи Google Test Framework.
