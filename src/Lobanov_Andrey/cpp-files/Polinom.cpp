#include "Polinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom pMonom = new TMonom(0, -1);
	pHead->SetDatValue(pMonom);
	for (int i = 0; i < km; i++)
	{
		pMonom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(pMonom);
	}
}

TPolinom & TPolinom::operator+(TPolinom &q) // �������� ���������
{
	PTMonom pm, qm, tm;
	Reset();
	q.Reset();
	while (1)
	{
		pm = GetMonom();
		qm = q.GetMonom();
		if (pm->Index < qm->Index)
		{
			// ������� ������ pm ������ �������� ������ qm => ���������� ������ qm  � ������� p
			tm = new TMonom(qm->Coeff, qm->Index);
			InsCurrent(tm);
			q.GoNext();
		}
		else if (pm->Index > qm->Index)
			GoNext();
		else // ������� ������� �����
		{
			if (pm->Index == -1) // ������ �� ������ ���� �������������
				break;
			pm->Coeff += qm->Coeff;
			if (pm->Coeff != 0)
			{
				GoNext();
				q.GoNext();
			}
			else // �������� ������ � ������� �������������
			{
				DelCurrent();
				q.GoNext();
			}
		}
	}
	return *this;
}

TPolinom::TPolinom(TPolinom &q) // ����������� �����������
{
	PTMonom pMonom = new TMonom(0, -1);
	pHead->SetDatValue(pMonom);
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		pMonom = q.GetMonom();
		InsLast(pMonom->GetCopy());
	}
	q.Reset();
}

TPolinom & TPolinom::operator=(TPolinom &q) // ������������
{
	if (this != &q)
	{
		PTMonom pMonom;
		DelList();
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			pMonom = q.GetMonom();
			InsLast(pMonom->GetCopy());
		}
	}
	return *this;
}

ostream& operator<<(ostream &os, TPolinom &q)
{
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
		cout << *q.GetMonom() << endl;
	return os;
}

bool TPolinom::operator==(TPolinom &q) // ��������� ���������
{
	if (GetListLength() != q.GetListLength())
		return false;
	else
	{
		PTMonom Mon1, Mon2;
		Reset(); q.Reset();
		while (!IsListEnded())
		{
			Mon1 = GetMonom();
			Mon2 = GetMonom();
			if (*Mon1 == *Mon2)
			{
				GoNext(); q.GoNext();
			}
			else
				return false;
		}
		return true;
	}
}

bool TPolinom::operator!=(TPolinom &q) // ��������� ���������
{
	return !(*this == q);
}

double TPolinom::CalculatePoly(int x, int y, int z)
{
	double res = 0;
	PTMonom mon;
	int indx, indy, indz;
	if (ListLen)
	{
		for (Reset(); !IsListEnded(); GoNext())
		{
			mon = GetMonom();
			indx = mon->Index / 100;
			indy = (mon->Index % 100) / 10;
			indz = mon->Index % 10;
			res += mon->Coeff*pow(x, indx)*pow(y, indy)*pow(z, indz);
		}
	}
	return res;
}