#include "Polinom.h"
#include <iostream>
using namespace std;

int main()
{
	cout << "Polinom test" << endl;
	int ms1[][2] = { {1, 543}, {3, 432}, {5, 321}, {7, 210}, {9, 100} };
	int mn1 = sizeof(ms1) / (2 * sizeof(int));
	TPolinom p(ms1, mn1);
	cout << "Polinom 1: " << endl << p;

	int ms2[][2] = { { 2, 643 },{ 4, 431 },{ -5, 321 },{ 8, 110 },{ 10, 50 } };
	int mn2 = sizeof(ms2) / (2 * sizeof(int));
	TPolinom q(ms2, mn2);
	cout << "Polinom 2: " << endl << q;

	TPolinom r = p + q;
	cout << "Result: " << endl << r;

	return 0;
}