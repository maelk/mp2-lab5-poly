#include "Polinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom Monom = new TMonom(0, -1);
	pHead->SetDatValue(Monom);
	for (int i = 0; i < km; i++)
	{
		Monom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(Monom);
	}
}

TPolinom::TPolinom(TPolinom &q)
{
	PTMonom Monom = new TMonom(0, -1);
	pHead->SetDatValue(Monom);
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		Monom = q.GetMonom();
		InsLast(Monom->GetCopy());
	}
}

TPolinom& TPolinom:: operator+(TPolinom &q)
{
	PTMonom pm, qm, rm;
	q.Reset();
	Reset();

	while (true) {
		pm = GetMonom();
		qm = q.GetMonom();
		if (pm->Index < qm->Index) {
			rm = new TMonom(qm->Coeff, qm->Index);
			InsCurrent(rm);
			q.GoNext();
		}
		else if(pm->Index > qm->Index) {
			GoNext();
		}
		else {
			if (pm->Index == -1)
				break;
			pm->Coeff += qm->Coeff;
			if (pm->Coeff != 0) {
				GoNext();
				q.GoNext();
			}
			else {
				DelCurrent();
				q.GoNext();
			}
		}
	}
	return *this;
}


TPolinom & TPolinom:: operator=(TPolinom &q)
{
	DelList();

	if ( (&q!=nullptr) && (&q != this) )
	{
		PTMonom Mon = new TMonom(0, -1);
		pHead->SetDatValue(Mon);
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			Mon = q.GetMonom();
			InsLast(Mon->GetCopy());
		}
	}
	return *this;
}

ostream& operator<<(ostream &os, TPolinom &q)
{
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
		cout << *q.GetMonom() << endl;
	return os;
}